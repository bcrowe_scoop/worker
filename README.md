# Scoop Worker Process
This repo holds the code for the worker process. The worker process processes video, images, and notification for scoop

## Event Type Reference
* process_video
    * **Trigger**: New video was uploaded
    * **Data Model**: 
        ```
        {
            "action": "process_video",
            "user_uuid": "f02h3032fsdlfsd023",
            "data": {
                "_id
                "user_id: "f02h3032fsdlfsd023",
                "bucketName": "videos",
                "objectName": "5fe64b23276c73851c6d871a.mp4",
                "size": "16265175",
                "metaData": { fieldName: 'video' },
                "storage_type": "minio",
                "etag": "f918225a7afde5c541cd788f5caecd47"
            },
            "timestamp": new Date()
            
        }
        ```
    * **Actions**:
        * Trancode Video to h264 using FFMPEG
        * Generate 3 Thumbnails and update video in DB
    * **Notify**: 
        * Publish job complete notification to Redis for SocketIO
        * Send email notification if enabled on user profile.
* delete_video
    * **Trigger**: Video data deleted from database
    * **Data Model**: 
        ```
        {
            "action": "delete_video",
            "user_uuid": "f02h3032fsdlfsd023",
            "data": {
                "_id": "5fe64b23276c73851c6d871a",
                "user_id: "f02h3032fsdlfsd023",
                "thumbnails": [
                    "5fe64b23276c73851c6d871a_thumb_0.jpg",
                    "5fe64b23276c73851c6d871a_thumb_1.jpg",
                    "5fe64b23276c73851c6d871a_thumb_2.jpg"
                ],
            },
            "timestamp": new Date()
            
        }
        ```
    * **Actions**:
        * Delete Video form storage
    * **Notify**: 
        * Publish job complete notification to Redis for SocketIO
        * Send email notification if enabled on user profile.
## Docker Commands
* Build
```
docker build -t bcrowe/scoop_worker .
```

* Run
```
docker run --env='NEO4J_HOST=172.16.1.155' --env='RABBITMQ_HOST=172.16.1.155' --env='REDIS_HOST=172.16.1.155' --env='MINIO_ENDPOINT=172.16.1.155' bcrowe/scoop_worker
```

## Docker Environment Variables
The following varibles are available to configure the image
```
host: APP_HOST
port: APP_PORT
jwt_secret: APP_JWT_SECRET
api_auth: APP_API_AUTH
session_time: APP_SESSION_TIME
tmp_storage_directory: APP_TMP_STORAGE_DIRECTORY
media_service_provider:
  destination: MSP_DESTINATION
  image_base_path: MSP_IMAGE_BASE_PATH
  dash_base_path: MSP_DASH_BASE_PATH
  hls_base_path: MSP_HLS_BASE_PATH
minio:
  endPoint: MINIO_ENDPOINT
  port: MINIO_PORT
  useSSL: MINIO_USESSL
  accessKey: MINIO_ACCESSKEY
  secretKey: MINIO_SECRETKEY
  bucketName: MINIO_BUCKETNAME
  image_bucket: MINIO_IMAGE_BUCKET
redis:
  host: REDIS_HOST
  port: REDIS_PORT
rabbitmq:
  host: RABBITMQ_HOST
  port: RABBITMQ_PORT
  worker_queue: RABBITMQ_WORKER_QUEUE
database:
  host: DATABASE_HOST
  name: DATABASE_NAME
  port: DATABASE_PORT
  user: DATABASE_USER
  password: DATABASE_PASSWORD
```