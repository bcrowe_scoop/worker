require('dotenv').config()
const rabbitmq = require('./services/rabbitmq')
const workers = require('./services/workers')
const io = require('./services/socketio_emitter')
    // Start RabbitMQ
rabbitmq.start(conn => {

    // Start the SocketIO Emmiter
    console.log('[REDIS] Starting socketio_emitter')
        // Start the worker
    console.log(`[AMQP] Starting Workers`)
    workers.start(conn, io)
})