const minio = require('../minio');
const config = require('../config')
module.exports = (io, event) => {
    // Remove video
    // Send /video_processing/started event in SocketIO
    var video_object = `${event.data.uuid}.mp4`
    console.log(`[MINIO] Deleting video: ${video_object} from bucket: ${config.minio.bucketName}`);
    return minio.removeObject(video_object, config.minio.bucketName)
        .then(() => {
            var promise_array = []
            if (event.data.thumbnails && typeof event.data.thumbnails != 'undefined') {
                event.data.thumbnails.forEach(t => {
                    console.log(`[MINIO] Deleting thumbnail: ${t} from bucket: ${config.minio.image_bucket}`);
                    promise_array.push(minio.removeObject(t, config.minio.image_bucket))
                })
                return Promise.all(promise_array)
            }

        })
}