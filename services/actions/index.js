const minio = require('../minio')

module.exports = {
    process_video: require('./process_video'),
    delete_video: require('./delete_video')
}