const path = require('path')
const fs = require('fs')
const config = require('../config')
const minio = require('../minio');
const ffmpeg = require('../ffmpeg');
const ds = require('../ds');

module.exports = (io, event) => {
        return new Promise((resolve, reject) => {

            // Send /video_processing/started event in SocketIO
            console.log(`[REDIS] Sending /video_processing/started event in SocketIO RoomID ${event.user_uuid} via REDIS`);
            io.to(event.user_uuid).emit('/video_processing/started', event);

            // Set new video name
            var new_video_name = `${event.data.uuid}.mp4`
            var new_video_file_path = path.join(config.temp_storage_path, new_video_name)
            var origina_video_file_path = path.join(config.temp_storage_path, event.data.objectName)

            // Get video from MINIO
            console.log(config.minio)
            console.log(event.data)
            minio.getObject(event.data.objectName, config.minio.bucketName, origina_video_file_path)

            // Generate Thumbnails
            .then(local_path => {
                return ffmpeg.generateThumbnails(local_path, 3, event.data.uuid)
            })

            // Upload Thumbnails and remove
            .then(thumbnail_array => {
                    var promise_array = []
                    thumbnail_array.forEach(t => {
                        promise_array.push(minio.putObject(t.thumbnail_name, config.minio.image_bucket, t.thumbnail_file_path, { mimetype: 'image/jpeg' }))
                    })
                    return Promise.all(promise_array).then(() => {
                        thumbnail_array.forEach(t => {
                            // Remove local thumbnail images
                            try {
                                console.log(`[FS] Removing thumbnail ${t.thumbnail_file_path}`)
                                fs.unlinkSync(t.thumbnail_file_path)
                                console.log(`[FS] Thumbnail remmoved ${t.thumbnail_file_path}`)
                            } catch (err) {
                                return reject(err)
                            }
                        })
                        return thumbnail_array
                    })
                })
                // Update video in db and send notification of thumbnails
                .then(thumbnail_array => {
                    var con_thumb_array = thumbnail_array.map(t => t.thumbnail_name)

                    // Update Video in Neo4J Database
                    const Video = ds('Video')
                    return Video.update(event.data.uuid, { thumbnails: con_thumb_array, poster: con_thumb_array[0], title: event.data.originalname }).then(() => {
                        // Send /video_processing/thumbnails event in SocketIO
                        console.log(`[REDIS] Sending /video_processing/thumbnails event in SocketIO RoomID ${event.user_uuid} via REDIS`);
                        io.to(event.user_uuid).emit('/video_processing/thumbnails', event);
                        return thumbnail_array
                    })
                })
                // Transcode video
                .then(() => {
                    return ffmpeg.transcode(origina_video_file_path, new_video_file_path)
                })
                // Upload new transcoded video
                .then(new_file_path => {
                    return minio.putObject(new_video_name, config.minio.bucketName, new_file_path, { mimetype: 'video/mp4' })
                })

            // Update video status in Neo4j
            .then(() => {
                const Video = ds('Video')
                Video.update(event.data.uuid, { status: 'ready' }).then(() => {
                    // Send /video_processing/complete event in SocketIO
                    console.log(`[Database] Updating finished.`)
                    console.log(`[REDIS] Sending /video_processing/complete event in SocketIO RoomID ${event.user_uuid} via REDIS`);
                    io.to(event.user_uuid).emit('/video_processing/complete', event);
                    return event
                })
            })

            // Remove old videos and send notification
            .then(() => {
                // Remove Old Original Video
                try {
                    console.log(`[FS] Removing Original Video`)
                    fs.unlinkSync(origina_video_file_path)
                    console.log(`[FS] Original Video Removed`)
                } catch (err) {
                    return reject(err)
                }

                // Remove New Transcoded Video
                try {
                    console.log(`[FS] Removing New Transcoded Video`)
                    fs.unlinkSync(new_video_file_path)
                    console.log(`[FS] New Transcoded Video Removed`)
                } catch (err) {
                    return reject(err)
                }
                return resolve(minio.removeObject(event.data.objectName, config.minio.bucketName).then(() => {
                    return event
                }))
            })
        });

    }
    // FFMPEG: .\ffmpeg.exe -i custom.mp4 -c:v libx264 -preset fast -crf 22 -c:a copy output.mp4