const { ObjectID } = require('mongodb');
module.exports = {
    videos: db => {
        var col = db.collection('videos')
        return {
            update(_id, doc) {
                return new Promise((resolve, reject) => {
                    col.updateOne({ _id: ObjectID(_id) }, { $set: doc }, (err, update_results) => {
                        if (err) {
                            return reject(err)
                        }
                        return resolve(update_results)
                    })
                })
            }
        }
    }
}