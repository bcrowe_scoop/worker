const config = require('../services/config')
var MongoClient = require('mongodb').MongoClient;
var url = ''
var db = false
if (config.database.user && config.database.user != '') {
    url = `mongodb://${encodeURIComponent(config.database.user)}:${encodeURIComponent(config.database.password)}@${encodeURIComponent(config.database.host)}:${config.database.port}/?authSource=admin`;
} else {
    url = `mongodb://${encodeURIComponent(config.database.host)}:${config.database.port}/${encodeURIComponent(config.database.name)}`;
}

function connect(cb) {
    console.log(`[MongoDB] Connecting to MongoDB at ${url}`)
    MongoClient.connect(url, { useUnifiedTopology: true }, function (err, conn) {
        if (err) {
            console.log('[MongoDB] connection error', err.message)
            return setTimeout(connect, 2000)
        } else {
            db = conn.db(config.database.name)
            console.log('[MongoDB] connection successful')
            return cb(err, db)
        }
    })
}

module.exports = cb => {
    if (!db) {
        connect(cb)
    } else {
        return cb(null, db)
    }

}