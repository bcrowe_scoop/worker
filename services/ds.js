const driver = require('./neo4j');
const { v4: uuidv4 } = require('uuid');
const _ = require('lodash');

function map_records(records) {
    return records.map(r => {
        return r._fields[0]
    })
}

function map_record(records) {
    if (records.length > 0) {
        return records[0]._fields[0]

    }
    return {}
}

module.exports = label => {
    return {
        async select() {
            const session = driver.session()
            const query = `
                MATCH (u:${label} )RETURN u { .*, videos:[ (u)--(v:TestVideo) | v {.*} ], playlists:[ (u)--(p:TestPLaylist) | p {.*} ]}
            `
            try {
                results = await session.run(query)
                return map_records(results.records)
            } finally {
                session.close()
            }
        },
        async selectOne(uuid) {
            const session = driver.session()
            const params = { uuid }
            const query = `
            MATCH (u:${label} {uuid: $uuid})RETURN u { .*, videos:[ (u)--(v:TestVideo) | v {.*} ]}
            `
            try {
                results = await session.run(query, params)
                return map_record(results.records)
            } finally {
                session.close()
            }
        },
        async insert(doc) {
            doc.uuid = uuidv4()
            const session = driver.session()
            const params = { doc }
            const query = `
                CREATE (u:${label} $doc) RETURN u{.*}
            `
            try {
                results = await session.run(query, params)
                return map_record(results.records)
            } finally {
                session.close()
            }
        },
        async insertWithRelationship(doc, relationship) {
            // relationship reference
            // var relationship = {
            //     label: 'Label of the node to create relationship to',
            //     name: 'Type of relationship. This is the name of the relationship',
            //     uuid: 'uuid of the node to create relationship',
            //     outbound: 'Boolean: Whether to create the relationship as outbound or inbound'
            // }
            doc.uuid = uuidv4()
            const session = driver.session()
            const params = { doc, uuid: relationship.uuid }
            var query = ''
            if (relationship.outbound) {
                query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    CREATE (u:${label} $doc)
                    CREATE (u)-[r:${relationship.name}]->(n)
                    RETURN u{.*}
                `
            } else {
                query = `
                    MATCH (n:${relationship.label} {uuid: $uuid})
                    CREATE (u:${label} $doc)
                    CREATE (u)<-[r:${relationship.name}]-(n)
                    RETURN u{.*}
                `
            }

            try {
                results = await session.run(query, params)
                return map_record(results.records)
            } finally {
                session.close()
            }

        },
        async update(uuid, doc) {
            doc = _.omit(doc, 'uuid')
            const session = driver.session()
            const params = { doc, uuid }
            const query = `
                MATCH (p:${label} { uuid: $uuid })
                SET p += $doc
                RETURN p {.*}
            `
            try {
                results = await session.run(query, params)
                return map_record(results.records)
            } finally {
                session.close()
            }
        },
        async delete(uuid) {
            const session = driver.session()
            const params = { uuid }
            const query = `
                MATCH (p:${label} { uuid: $uuid })
                DELETE p
            `
            try {
                results = await session.run(query, params)
                return results
            } finally {
                session.close()
            }
        }
    }
}