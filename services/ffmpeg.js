const cp = require('child_process');
const path = require('path')

const fs = require('fs')
const config = require('./config')
const { getVideoDurationInSeconds } = require('get-video-duration')
var moment = require("moment");
var momentDurationFormatSetup = require("moment-duration-format");


function timecodes(duration, total_frames) {
    duration = parseInt(duration)
    total_frames = parseInt(total_frames)
    var interval = (duration - 2) / total_frames
    let tc = []
    var ss_1 = moment.duration(1, 'seconds')
    tc.push(ss_1.format('hh:mm:ss', { trim: false }))
    for (let index = 1; index < total_frames; index++) {
        let seconds = index * interval
        tc.push(moment.duration(seconds, 'seconds').format('hh:mm:ss', { trim: false }))
    }
    return tc
}

function genThumbnail(video_file_path, thumbnail_directory, thumbnail_name, timecode) {
    return new Promise((resolve, reject) => {
        var thumbnail_file_path = path.join(thumbnail_directory, thumbnail_name)
        var ffmpeg_ss = cp.spawn('./ffmpeg.exe', ['-ss', timecode, '-i', video_file_path, '-vframes', '1', '-q:v', '2', `${thumbnail_file_path}`]);
        ffmpeg_ss.on('error', err => {
            return reject(err)
        })
        ffmpeg_ss.on('close', () => {
            return resolve({ thumbnail_file_path, thumbnail_name })
        })
    })
}
module.exports = {
    transcode(original_file_path, new_file_path) {
        return new Promise((resolve, reject) => {
            console.log(`[FFMPEG] Transcoding video: ${original_file_path} -> ${new_file_path} starting`);
            try {
                var ffmpeg = cp.spawn('./ffmpeg.exe', ['-i', `./${original_file_path}`, '-c:v', 'libx264', '-preset', 'fast', '-crf', '22', '-c:a', 'aac', `./${new_file_path}`]);
            } catch (error) {
                reject(error)
            }

            // Trancode Video
            ffmpeg.stderr.on('data', data => console.log(`[FFMPEG] ${data.toString()}`))
            ffmpeg.stdout.on('data', data => console.log(`[FFMPEG] ${data.toString()}`))

            ffmpeg.on('error', err => {
                console.error(`[FFMPEG] Error transcoding error.`)
                console.log(err);
                reject(err)
            })

            ffmpeg.on('close', (code) => {
                console.log(`[FFMPEG] child process exited with code ${code}`);
                return resolve(new_file_path)
            })
        });
    },
    generateThumbnails(video_file_path, thumbnail_amount, video_id) {
        return new Promise((resolve, reject) => {
            return getVideoDurationInSeconds(video_file_path).then(duration => {
                // Get timecodes for thumbnails
                var tc = timecodes(duration, thumbnail_amount)
                var promise_array = []

                // loop over timecode and generate a thumbnail for each timecode
                tc.forEach((t, i) => {
                    console.log(`[FFMPEG] Generating Thumbnail ${i}`)

                    // Set Thumbnail Name
                    let new_tumbnail_name = `${video_id}_thumb_${i}.jpg`

                    // Generate Thumbnail -> add to promise array
                    promise_array.push(genThumbnail(video_file_path, config.temp_storage_path, new_tumbnail_name, t))
                })
                return resolve(Promise.all(promise_array))
            })

        });

    }

}