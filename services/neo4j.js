const neo4j = require('neo4j-driver')
const config = require('./config')
const driver = neo4j.driver(`neo4j://${config.neo4j.host}:${config.neo4j.port}`, null, { disableLosslessIntegers: true })

module.export = driver