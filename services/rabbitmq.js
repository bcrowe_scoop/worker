var amqp = require('amqplib/callback_api');
var config = require('./config')
// if the connection is closed or fails to be established at all, we will reconnect
var amqpConn = null;
var pubChannel = null;
var offlinePubQueue = [];
function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}

function publish(exchange, routingKey, data) {
    // Parse data into Buffer from JSON string
    console.log(`[AMQP] Publishing job to Exchange: ${exchange}, Worker queue: ${routingKey}`)
    try {
        var content = Buffer.from(JSON.stringify(data))
    } catch (error) {
        console.error("[AMQP] publish error:", error.message);
    }

    try {
        pubChannel.publish(exchange, routingKey, content, { persistent: true },
            function (err, ok) {
                if (err) {
                    console.error("[AMQP] publish error:", err.message);
                    offlinePubQueue.push([exchange, routingKey, content]);
                    pubChannel.connection.close();
                }
            });
    } catch (e) {
        console.error("[AMQP] publish error:", e.message);
        offlinePubQueue.push([exchange, routingKey, content]);
    }
}



function start(cb) {
    if (amqpConn) {

        // Call the callback if one is given
        if (cb && typeof cb == 'function') {
            cb(amqpConn)
        }
    } else {
        var url = `amqp://${config.rabbitmq.host}` + "?heartbeat=60"

        console.log(`[AMQP] Connecting to RabbitMQ at ${url}`)
        amqp.connect(url, function (err, conn) {
            if (err) {
                console.error("[AMQP] connection error:", err.message);
                return setTimeout(start, 1000);
            }
            conn.on("error", function (err) {
                if (err.message !== "Connection closing") {
                    console.error("[AMQP] connection error", err.message);
                }
            });
            conn.on("close", function () {
                console.error("[AMQP] reconnecting");
                return setTimeout(start, 1000);
            });

            console.log("[AMQP] connected");
            amqpConn = conn;

            // Start Publisher
            amqpConn.createConfirmChannel(function (err, ch) {
                if (closeOnErr(err)) return;
                ch.on("error", function (err) {
                    console.error("[AMQP] channel error:", err.message);
                });
                ch.on("close", function () {
                    console.log("[AMQP] channel closed");
                });

                pubChannel = ch;

                // Call the callback if one is given
                if (cb && typeof cb == 'function') {
                    cb(amqpConn)
                }

                while (true) {
                    var m = offlinePubQueue.shift();
                    if (!m) break;
                    publish(m[0], m[1], m[2]);
                }
            });

        });
    }
}

module.exports = {
    start,
    publish
}