const config = require('./config');
const socketio_emitter = require('socket.io-emitter')
const io = socketio_emitter({ host: config.redis.host, port: config.redis.port });

io.redis.on('error', err => {
    console.log(err)
})

module.exports = io