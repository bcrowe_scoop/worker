const config = require('./config')
const actions = require('./actions')

function closeOnErr(err) {
    if (!err) return false;
    console.error("[AMQP] error", err);
    amqpConn.close();
    return true;
}

function workerQueue(ch, io) {
    ch.assertQueue(config.rabbitmq.worker_queue, { durable: true }, function(err, _ok) {
        if (closeOnErr(err)) return;
        ch.consume(config.rabbitmq.worker_queue, processStream, { noAck: false });
        console.log("[AMQP] Worker is started");

    });

    // Process messages
    function processStream(msg) {
        console.log(`[AMQP] Received msg`)
        try {
            var event = JSON.parse(msg.content.toString())
        } catch (err) {
            console.error(`[AMQP] Error parsing message`, err.message)
            ch.nack(msg)
        }


        // event object must have .type, and .event. .type = the action to take
        if (event.action && typeof actions[event.action] == 'function') {
            console.log(`[Worker] Executing ${event.action}:`)

            // Process Message
            try {
                actions[event.action](io, event).then(res => {
                    ch.ack(msg)
                    console.log(res)
                }).catch(err => {
                    ch.nack(msg)
                    console.log(`[Worker] Error executing ${event.action}: ${err.message}`)
                })
            } catch (action_error) {
                console.log(`[Worker] Error executing ${event.action}: ${action_error.message}`)
                ch.nack(msg)
            }

        }
    }
}

module.exports = {
    start(amqpConn, io) {
        amqpConn.createChannel(function(err, ch) {
            if (closeOnErr(err)) return;
            ch.on("error", function(err) {
                console.error("[AMQP] channel error:", err.message);
            });
            ch.on("close", function() {
                console.log("[AMQP] channel closed");
            });
            ch.prefetch(10);
            workerQueue(ch, io)
        })
    }
}